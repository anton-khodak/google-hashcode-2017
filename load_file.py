FILES = ['input/kittens.in', ]
# V​ (1 ≤ V ≤ 10000) - the number of videos
# E (1 ≤ E ≤ 1000) - the number of endpoints
# R (1 ≤ R ≤ 1000000) - the number of request descriptions
# C (1 ≤ C ≤ 1000) - the number of cache servers
# X (1 ≤ X ≤ 500000) - the capacity of each cache server in megabytes


def line_to_ints(line):
    return [int(i) for i in line.split(' ')]


def load_file(file):
    with open(file, 'r') as f:
        lines = f.readlines()
        print(line_to_ints(lines[0]))
        V, E, R, C, X = line_to_ints(lines[0])
        video_sizes = {key: value for key, value in enumerate(line_to_ints(lines[1]))}   # dict of video_idx: video size

        # Endpoint section
        # endpoint:  {id: {"main_latency": latency, "caches": {cache_id: latency, }}}
        endpoints = dict()
        caches = dict()
        is_endpoint_line = True
        endpoint_id = 0
        for line_idx, line in zip(range(2, len(lines)), lines[2:]):
            if len(line_to_ints(line)) != 2:
                break
            if is_endpoint_line:
                is_endpoint_line = False
                latency, nmb_of_endpoints = line_to_ints(line)
                counter = 0
                endpoints[endpoint_id] = {"main_latency": latency, "caches": dict()}
                continue

            if counter < nmb_of_endpoints:
                cache_idx, latency_cache = line_to_ints(line)
                endpoints[endpoint_id]["caches"][cache_idx] = latency_cache
                if caches.get(cache_idx, ''):
                    caches[cache_idx].append(endpoint_id)
                else:
                    caches[cache_idx] = [endpoint_id]
                counter += 1
            else:
                is_endpoint_line = True
                endpoint_id += 1

            if is_endpoint_line:
                is_endpoint_line = False
                latency, nmb_of_endpoints = line_to_ints(line)
                counter = 0
                endpoints[endpoint_id] = {"main_latency": latency, "caches": dict()}
                continue

        print("Number of endpoints", len(endpoints))

        # Requests section
        requests = dict()    # {endpoint_idx: {video_id: nmb_requests}}
        for line in lines[line_idx:]:
            video_idx, cache_idx, nmb_requests = line_to_ints(line)
            try:
                requests[cache_idx][video_idx] = nmb_requests
            except KeyError:
                requests[cache_idx] = dict()
                requests[cache_idx][video_idx] = nmb_requests

        """
        V ​ ( 1 ≤ V ≤ 10000) - the number of videos
        ● E ( 1 ≤ E ≤ 1000) - the number of endpoints
        ● R ( 1 ≤ R ≤ 1000000) - the number of request descriptions
        ● C ( 1 ≤ C ≤ 1000) - the number of cache servers
        ● X ( 1 ≤ X ≤ 500000) - the capacity of each cache server in megabytes
        """

        return video_sizes, requests, endpoints, V, E, R, C, X, caches










