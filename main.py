import itertools

import logging


class Video:
    def __init__(self, id, size):
        self.id = id
        self.size = size


class Endpoint:
    def __init__(self, id, video_requests, cacheServer_latency, datacenter_latency):
        self.id = id
        self.video_requests = video_requests
        self.cacheServer_latency = cacheServer_latency
        self.datacenter_latency = datacenter_latency

    def get_latency(self, server):
        return self.cacheServer_latency.get(server.id, -1)

    def is_connected(self, server):
        return self.get_latency(server) > 0

    def get_number_of_requests(self, video):
        return self.video_requests.get(video.id, 0)

    # def calculate_best_video_latency(self, video):
    #     valid_servers_latencies = []
    #     for server in self.cacheServer_latency:
    #         if self.is_connected(server) and server.has_cached(video):
    #             valid_servers_latencies.append(self.get_latency(server))
    #
    #     return min(self.datacenter_latency, valid_servers_latencies)

    def get_metrics_value(self, server, video, related_endpoints):
        logger.debug('Calculating metrics for server {0} video {1}'.format(server.id, video.id))
        # upper = self.get_number_of_requests(video)
        upper = self.video_requests.get(video.id, 0)
        for endpoint in related_endpoints:
            upper += endpoint.video_requests.get(video.id, 0)
        return upper / video.size / self.get_latency(server)

    def get_partial_metrics_value(self, server, video):
        return self.video_requests.get(video.id, 0) / video.size / self.get_latency(server)


class CacheServer:
    def __init__(self, id, capacity):
        self.id = id
        self.capacity = capacity
        self.videos = list()
        self.current_load = 0

    def place(self, video):
        self.videos.append(video)
        self.current_load += video.size
        task.cache_servers_used.update({self})

    def has_cached(self, video):
        return video in self.videos

    def can_contain(self, video):
        return self.current_load + video.size <= self.capacity

    def get_related_endpoints(self, excluding=None):
        related_endpoints = []
        for endpoint in task.endpoints:
            if endpoint.is_connected(self):
                if excluding:
                    if excluding.id != endpoint.id:
                        related_endpoints.append(endpoint)
                else:
                    related_endpoints.append(endpoint)
        return related_endpoints


class Main:
    def __init__(self, endpoints, servers, videos):
        self.endpoints = endpoints
        self.servers = servers
        self.videos = {video.id: video for video in videos}
        self.cache_servers_used = set()

    # def metrics(self):
    #     time_saved_total = 0
    #     for endpoint in self.endpoints:
    #         time_saved_for_endpoint = 0
    #         for video in endpoint.video_requests.keys():
    #             number_of_requests = endpoint.get_number_of_requests(video)
    #             best_video_latency = endpoint.calculate_best_video_latency(video)
    #             video_latency = endpoint.datacenter_latency - best_video_latency
    #             time_saved_for_endpoint += number_of_requests * video_latency
    #         time_saved_total += time_saved_for_endpoint
    #     return time_saved_total * 1000 / self._get_total_number_of_requests()

    def _get_total_number_of_requests(self):
        total = 0
        for endpoint in self.endpoints:
            total += sum(endpoint.video_requests.values())
        return total

    def write_answer(self, path):
        with open(path, 'w') as f:
            f.write('{0}\n'.format(len(self.cache_servers_used)))
            for server in self.cache_servers_used:
                videos = ' '.join(str(video.id) for video in server.videos)
                f.write('{0} {1}\n'.format(server.id, videos))

    def get_endpoint_by_id(self, id):
        for endpoint in self.endpoints:
            if endpoint.id == id:
                return endpoint

    def get_video_by_id(self, id):
        return self.videos.get(id)

    def get_server_by_id(self, id):
        for server in self.servers:
            if server.id == id:
                return server

    def get_structure(self):
        structure = [[None for j in range(End)] for i in range(Cac)]
        """
        structure[server.id][endpoint.id] == [
                    (video, value)
                    (video, value)
                ]

        """
        for endpoint in self.endpoints:
            logger.info('Endpoint {0}'.format(endpoint.id))
            for cacheServerLatency in endpoint.cacheServer_latency.keys():
                server = task.get_server_by_id(cacheServerLatency)
                logger.info('Cacheserver {0}'.format(server.id))
                logger.debug('Getting related endpoints')
                related_endpoints = server.get_related_endpoints(excluding=endpoint)
                values = []
                for video_id in endpoint.video_requests:
                    video = task.get_video_by_id(video_id)
                    values.append((video, endpoint.get_metrics_value(server, video, related_endpoints)))
                structure[server.id][endpoint.id] = values
        return structure

    def optimization(self, matrix):
        num_of_caches = len(self.servers)
        num_of_endpoints = len(self.endpoints)

        def sort_matrix_lists():
            for cache_id in range(num_of_caches):
                for endpoint_id in range(num_of_endpoints):
                    try:
                        matrix[cache_id][endpoint_id] = sorted(matrix[cache_id][endpoint_id],
                                                               key=lambda v: v[1],
                                                               reverse=True)
                    except TypeError:
                        # skipping Nones
                        pass

        def find_max_in_all_cache_endpoints():
            i_max = j_max = max_value = 0
            for cache_id in range(num_of_caches):
                for endpoint_id in range(num_of_endpoints):
                    try:
                        tmp = matrix[cache_id][endpoint_id][0][1]
                        if tmp > max_value:
                            max_value = tmp
                            i_max = cache_id
                            j_max = endpoint_id
                    except TypeError:
                        # skip nones
                        pass
                    except IndexError:
                        # all items deleted
                        pass
            return i_max, j_max, max_value

        def find_el_in_matrix_list(i, j, video):
            for el in matrix[i][j]:
                if el[0] == video.id:
                    return matrix[i][j].index(el)

        def is_empty_structure():
            is_empty = True
            for cache_id in range(num_of_caches):
                for endpoint_id in range(num_of_endpoints):
                    value = matrix[cache_id][endpoint_id]
                    if value is not None and value != []:
                        return False
            return is_empty

        while not is_empty_structure():

            sort_matrix_lists()
            i_max, j_max, value_max = find_max_in_all_cache_endpoints()

            server = task.get_server_by_id(i_max)
            endpoint = task.get_endpoint_by_id(j_max)
            print("i_max", i_max)
            print("j_max", j_max)
            print()

            try:
                video = matrix[i_max][j_max][0][0]
                if server.can_contain(video) and not server.has_cached(video):
                    server.place(video)
                del matrix[i_max][j_max][0]
            except IndexError:
                pass

            related_endpoints = server.get_related_endpoints()
            sub_score = endpoint.get_partial_metrics_value(server, video)
            for related_endpoint in related_endpoints:
                if endpoint.video_requests.get(video.id, 0) > 0:
                    index_of_video_requests_pair = find_el_in_matrix_list(server.id, related_endpoint.id, video)
                    if index_of_video_requests_pair:
                        matrix[i_max][related_endpoint.id][index_of_video_requests_pair] -= sub_score


# V​ (1 ≤ V ≤ 10000) - the number of videos
# E (1 ≤ E ≤ 1000) - the number of endpoints
# R (1 ≤ R ≤ 1000000) - the number of request descriptions
# C (1 ≤ C ≤ 1000) - the number of cache servers
# X (1 ≤ X ≤ 500000) - the capacity of each cache server in megabytes

from datetime import datetime
logger = logging.getLogger('Main')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
logger.addHandler(ch)
start = datetime.now()
logger.info('Start: ', start)


from load_file import load_file
FILES = [
    # 'input/me_at_the_zoo.in',
    # 'input/kittens.in',
    # 'input/trending_today.in',
    # 'input/videos_worth_spreading.in'
         ]


for file in FILES:
    logger.info('Reading file {0}'.format(file))
    video_sizes, requests, endpoints, Vid, End, Req, Cac, Xcapacity = load_file(file)
    logger.info('Instantiating objects')
    videos = [Video(video_size[0], video_size[1]) for video_size in video_sizes.items()]
    servers = [CacheServer(id, Xcapacity) for id in range(Cac)]
    endpoints_list = [Endpoint(endpoint[0],
                               datacenter_latency=endpoint[1]['main_latency'],
                               cacheServer_latency=endpoint[1]['caches'],
                               video_requests=requests[endpoint[0]]) for endpoint in endpoints.items()]

    task = Main(endpoints_list, servers, videos)
    logger.info('Getting data structure')
    structure = task.get_structure()
    logger.info('Optimizing')
    task.optimization(structure)
    path = file.split('/')[1].replace('.in', '.txt')
    task.write_answer(path)
finished = datetime.now()
logger.info('Finished: ', finished)
logger.info('Took: ', finished - start)
