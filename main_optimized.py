import logging
from datetime import datetime

from sortedcontainers import SortedListWithKey


class Video:
    def __init__(self, id, size):
        self.id = id
        self.size = size


class Endpoint:
    def __init__(self, id, video_requests, cacheServer_latency, datacenter_latency):
        self.id = id
        self.video_requests = video_requests
        self.cacheServer_latency = cacheServer_latency
        self.datacenter_latency = datacenter_latency

    def get_latency(self, id):
        return self.cacheServer_latency.get(id, 0)

    def is_connected(self, server):
        return self.get_latency(server.id) > 0

    def get_number_of_requests(self, video_id):
        return self.video_requests.get(video_id, 0)

    # def get_metrics_value(self, server, video_id, video_size, related_endpoints):
    #     logger.debug('Calculating metrics for server {0} video {1}'.format(server.id, video_id))
    #     upper = server.get_video_total_request(video_id)
    #     if upper <= 0:
    #         upper = 0ze
    #         for endpoint in related_endpoints:
    #             upper += endpoint.get_number_of_requests(video_id)
    #         server.video_total_request[video_id] = upper
    #     else:
    #         pass
    #     try:
    #         metric_value = upper / video_size / self.get_latency(server.id)
    #     except ZeroDivisionError:
    #         metric_value = 0
    #     return metric_value, server

    def get_partial_metrics_value(self, server, video):
        latency = self.get_latency(server)
        if latency > 0:
            metric_value = self.video_requests.get(video.id, 0) / video.size / latency
        else:
            metric_value = 0
        return metric_value


class CacheServer:
    def __init__(self, id, capacity, related_endpoints):
        self.id = id
        self.capacity = capacity
        self.videos = list()
        self.current_load = 0
        self.related_endpoints = related_endpoints
        self.video_total_request = dict()

    def place(self, video):
        self.videos.append(video)
        self.current_load += video.size
        task.cache_servers_used.update({self})

    def has_cached(self, video):
        return video in self.videos

    def can_contain(self, video):
        return self.current_load + video.size <= self.capacity

    def get_video_total_request(self, video_id):
        return self.video_total_request.get(video_id, -1)

    def get_related_endpoints(self):
        return [task.get_endpoint_by_id(id) for id in self.related_endpoints]


class Main:
    def __init__(self, endpoints, servers, videos):
        self.endpoints = {endpoint.id: endpoint for endpoint in endpoints}
        self.servers = {server.id: server for server in servers}
        self.videos = {video.id: video for video in videos}
        self.cache_servers_used = set()

    def _get_total_number_of_requests(self):
        total = 0
        for endpoint in self.endpoints:
            total += sum(endpoint.video_requests.values())
        return total

    def write_answer(self, path):
        with open(path, 'w') as f:
            f.write('{0}\n'.format(len(self.cache_servers_used)))
            for server in self.cache_servers_used:
                videos = ' '.join(str(video.id) for video in server.videos)
                f.write('{0} {1}\n'.format(server.id, videos))

    def get_endpoint_by_id(self, id):
        return self.endpoints.get(id)

    def get_video_by_id(self, id):
        return self.videos.get(id)

    def get_server_by_id(self, id):
        return self.servers.get(id)

    def get_structure(self):
        structure = dict()

        for server in self.servers.values():
            logger.info('Server {0}'.format(server.id))
            related_endpoints = server.get_related_endpoints()
            for endpoint in related_endpoints:
                logger.info('Endpoint {0}'.format(endpoint.id))
                logger.debug('Getting related endpoints')
                values = SortedListWithKey(key=lambda v: v[1])
                for video_id in endpoint.video_requests:
                    video = task.get_video_by_id(video_id)

                    logger.debug('Calculating metrics for server {0} video {1}'.format(server.id, video_id))

                    # upper = endpoint.get_number_of_requests(video_id)
                    upper = server.get_video_total_request(video_id)
                    if upper <= 0:
                        upper = 0
                        for endpointDF in related_endpoints:
                            upper += endpointDF.get_number_of_requests(video_id)
                        server.video_total_request[video_id] = upper
                    else:
                        logger.debug('bingo')
                    metric_value = upper / video.size / endpoint.get_latency(server.id)
                    # metric_value = endpoint.get_metrics_value(server, video_id, video.size, related_endpoints)
                    values.add((video_id, metric_value))
                structure.setdefault(server.id, dict())[endpoint.id] = values

        return structure

    def optimization(self, matrix):
        num_of_caches = len(self.servers)
        num_of_endpoints = len(self.endpoints)

        def init_max():
            maxs = list()
            for cache_id in range(num_of_caches):
                for endpoint_id in range(num_of_endpoints):
                    try:
                        tmp = matrix[cache_id][endpoint_id][-1][1]

                        maxs.append((tmp, (cache_id, endpoint_id)))
                    except KeyError:
                        # skip nones
                        pass
                    except IndexError:
                        # all items deleted
                        pass

            return maxs

        def find_el_in_matrix_list(i, j, video_id):
            for el in matrix[i][j]:
                if el[0] == video_id:
                    return matrix[i][j].index(el)

        def find_element_in_cache(coord):
            for idx, el in enumerate(max_metric_in_all_cache_endpoints):
                if coord == el[1]:
                    return idx

        def is_empty_structure():
            is_empty = True
            for cache_id in range(num_of_caches):
                for endpoint_id in range(num_of_endpoints):
                    try:
                        value = matrix[cache_id][endpoint_id]
                        if value is not None and value != []:
                            return False
                    except KeyError:
                        pass
            return is_empty

        max_metric_in_all_cache_endpoints = SortedListWithKey(init_max(), key=lambda v: v[0])

        while not is_empty_structure():

            value_max, (i_max, j_max) = max_metric_in_all_cache_endpoints[-1]

            server = task.get_server_by_id(i_max)
            endpoint = task.get_endpoint_by_id(j_max)

            try:
                video = task.get_video_by_id(matrix[i_max][j_max][-1][0])
                if server.can_contain(video) and not server.has_cached(video):
                    server.place(video)
                matrix[i_max][j_max].pop()
                # Update cached maxes
                max_metric_in_all_cache_endpoints.pop()
                max_value = (matrix[i_max][j_max][-1][1], (i_max, j_max))  # new max value for matrix[i_max][j_max]
                max_metric_in_all_cache_endpoints.add(max_value)
            except IndexError:
                pass


            related_endpoints = server.get_related_endpoints()
            sub_score = endpoint.get_partial_metrics_value(server, video)
            # if
            if sub_score > 0:
                for related_endpoint in related_endpoints:
                    if endpoint.video_requests.get(video.id, 0) > 0:
                        index_of_video_requests_pair = find_el_in_matrix_list(server.id, related_endpoint.id, video.id)
                        if index_of_video_requests_pair:
                            tmp = matrix[i_max][related_endpoint.id].pop(index_of_video_requests_pair)

                            tmp = (tmp[0], tmp[1] - sub_score)
                            matrix[i_max][related_endpoint.id].add(tmp)
                            # update maxes cache
                            max_video = matrix[i_max][related_endpoint.id][-1]
                            max_el_index = find_element_in_cache((i_max, related_endpoint.id))
                            max_metric_in_all_cache_endpoints.pop(max_el_index)
                            max_metric_in_all_cache_endpoints.add((max_video[1], (i_max, related_endpoint.id)))






# V​ (1 ≤ V ≤ 10000) - the number of videos
# E (1 ≤ E ≤ 1000) - the number of endpoints
# R (1 ≤ R ≤ 1000000) - the number of request descriptions
# C (1 ≤ C ≤ 1000) - the number of cache servers
# X (1 ≤ X ≤ 500000) - the capacity of each cache server in megabytes


startTime = datetime.now()

logger = logging.getLogger('Main')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
logger.addHandler(ch)
start = datetime.now()
logger.info('Start: {0}'.format(start))

from load_file import load_file

FILES = [
    # 'input/me_at_the_zoo.in',
    # 'input/kittens.in',
    # 'input/trending_today.in',
    'input/videos_worth_spreading.in'
         ]


for file in FILES:
    basename = file.split('/')[1].replace('.in', '')
    logger.info('Reading file {0}'.format(file))
    video_sizes, requests, endpoints, Vid, End, Req, Cac, Xcapacity, caches = load_file(file)
    logger.info('Instantiating objects')
    videos = [Video(video_size[0], video_size[1]) for video_size in video_sizes.items()]
    servers = [CacheServer(id, Xcapacity, caches.get(id, list())) for id in range(Cac)]
    endpoints_list = [Endpoint(endpoint[0],
                               datacenter_latency=endpoint[1]['main_latency'],
                               cacheServer_latency=endpoint[1]['caches'],
                               video_requests=requests[endpoint[0]]) for endpoint in endpoints.items()]

    task = Main(endpoints_list, servers, videos)
    logger.info('Getting data structure')
    structure_file = basename + '.struct'
    structure = task.get_structure()
    logger.info('Allocating videos')
    task.optimization(structure)
    path = basename + '.txt'
    task.write_answer(path)

finished = datetime.now()
logger.info('Finished: {0}'.format(finished))
hours, remainder = divmod((finished - start).total_seconds(), 3600)
minutes, seconds = divmod(remainder, 60)
logger.info('Took: {0} hours, {1} minutes, {2} seconds'.format(hours, minutes, seconds))
